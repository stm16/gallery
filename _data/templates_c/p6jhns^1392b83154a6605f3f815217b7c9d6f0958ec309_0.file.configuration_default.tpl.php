<?php
/* Smarty version 3.1.29, created on 2016-09-02 22:11:56
  from "/config/www/gallery/admin/themes/default/template/configuration_default.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57c9f92c937375_25627337',
  'file_dependency' => 
  array (
    '1392b83154a6605f3f815217b7c9d6f0958ec309' => 
    array (
      0 => '/config/www/gallery/admin/themes/default/template/configuration_default.tpl',
      1 => 1467916582,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57c9f92c937375_25627337 ($_smarty_tpl) {
if (!is_callable('smarty_function_html_radios')) require_once '/config/www/gallery/include/smarty/libs/plugins/function.html_radios.php';
echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['combine_script'][0][0]->func_combine_script(array('id'=>'common','load'=>'footer','path'=>'admin/themes/default/js/common.js'),$_smarty_tpl);?>


<h2><?php echo l10n('Piwigo configuration');?>
 <?php echo $_smarty_tpl->tpl_vars['TABSHEET_TITLE']->value;?>
</h2>

<form method="post" name="profile" action="<?php echo $_smarty_tpl->tpl_vars['GUEST_F_ACTION']->value;?>
" id="profile" class="properties">

<div id="configContent">

<?php if ($_smarty_tpl->tpl_vars['GUEST_USERNAME']->value != 'guest') {?>
  <fieldset>
      <?php echo l10n('The settings for the guest are from the %s user',$_smarty_tpl->tpl_vars['GUEST_USERNAME']->value);?>

  </fieldset>
<?php }?>

  <fieldset>
    <legend><?php echo l10n('Preferences');?>
</legend>
    <input type="hidden" name="redirect" value="<?php echo $_smarty_tpl->tpl_vars['GUEST_REDIRECT']->value;?>
">

    <ul>
      <li>
        <span class="property">
          <label for="nb_image_page"><?php echo l10n('Number of photos per page');?>
</label>
        </span>
        <input type="text" size="4" maxlength="3" name="nb_image_page" id="nb_image_page" value="<?php echo $_smarty_tpl->tpl_vars['GUEST_NB_IMAGE_PAGE']->value;?>
">
      </li>

      <li>
        <span class="property">
          <label for="recent_period"><?php echo l10n('Recent period');?>
</label>
        </span>
        <input type="text" size="3" maxlength="2" name="recent_period" id="recent_period" value="<?php echo $_smarty_tpl->tpl_vars['GUEST_RECENT_PERIOD']->value;?>
">
      </li>

      <li>
        <span class="property"><?php echo l10n('Expand all albums');?>
</span>
        <?php echo smarty_function_html_radios(array('name'=>'expand','options'=>$_smarty_tpl->tpl_vars['radio_options']->value,'selected'=>$_smarty_tpl->tpl_vars['GUEST_EXPAND']->value),$_smarty_tpl);?>

      </li>

<?php if ($_smarty_tpl->tpl_vars['GUEST_ACTIVATE_COMMENTS']->value) {?>
      <li>
        <span class="property"><?php echo l10n('Show number of comments');?>
</span>
        <?php echo smarty_function_html_radios(array('name'=>'show_nb_comments','options'=>$_smarty_tpl->tpl_vars['radio_options']->value,'selected'=>$_smarty_tpl->tpl_vars['GUEST_NB_COMMENTS']->value),$_smarty_tpl);?>

      </li>
<?php }?>
      <li>
        <span class="property"><?php echo l10n('Show number of hits');?>
</span>
        <?php echo smarty_function_html_radios(array('name'=>'show_nb_hits','options'=>$_smarty_tpl->tpl_vars['radio_options']->value,'selected'=>$_smarty_tpl->tpl_vars['GUEST_NB_HITS']->value),$_smarty_tpl);?>

      </li>
    </ul>
  </fieldset>

  <p class="bottomButtons">
    <input type="hidden" name="pwg_token" value="<?php echo $_smarty_tpl->tpl_vars['PWG_TOKEN']->value;?>
">
    <input class="submit" type="submit" name="validate" value="<?php echo l10n('Submit');?>
">
    <input class="submit" type="reset" name="reset" value="<?php echo l10n('Reset');?>
">
  </p>

</div>

</form><?php }
}
