<?php
/* Smarty version 3.1.29, created on 2016-09-02 21:51:56
  from "/config/www/gallery/admin/themes/default/template/themes_installed.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57c9f47c9db8e9_00914057',
  'file_dependency' => 
  array (
    'ef8de9c776a28244f09206c76c7e18d06023b6aa' => 
    array (
      0 => '/config/www/gallery/admin/themes/default/template/themes_installed.tpl',
      1 => 1467916582,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/colorbox.inc.tpl' => 1,
  ),
),false)) {
function content_57c9f47c9db8e9_00914057 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/colorbox.inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
 

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('footer_script', array()); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['footer_script'][0][0]->block_footer_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

jQuery(document).ready(function() {
  $("a.preview-box").colorbox();
  
  jQuery('.showInfo').tipTip({
    'delay' : 0,
    'fadeIn' : 200,
    'fadeOut' : 200,
    'maxWidth':'300px',
    'keepAlive':true,
    'activation':'click'
  });
});
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['footer_script'][0][0]->block_footer_script(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<div class="titrePage">
  <h2><?php echo l10n('Installed Themes');?>
</h2>
</div>

<div id="themesContent">

<?php $_smarty_tpl->tpl_vars['field_name'] = new Smarty_Variable('null', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'field_name', 0);?> 
<?php
$_from = $_smarty_tpl->tpl_vars['tpl_themes']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_theme_0_saved_item = isset($_smarty_tpl->tpl_vars['theme']) ? $_smarty_tpl->tpl_vars['theme'] : false;
$_smarty_tpl->tpl_vars['theme'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['theme']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['theme']->value) {
$_smarty_tpl->tpl_vars['theme']->_loop = true;
$__foreach_theme_0_saved_local_item = $_smarty_tpl->tpl_vars['theme'];
?>
    
<?php if ($_smarty_tpl->tpl_vars['field_name']->value != $_smarty_tpl->tpl_vars['theme']->value['STATE']) {
if ($_smarty_tpl->tpl_vars['field_name']->value != 'null') {?>
    </div>
  </fieldset>
<?php }?>
  <fieldset>
    <legend>
<?php if ($_smarty_tpl->tpl_vars['theme']->value['STATE'] == 'active') {?>
      <?php echo l10n('Active Themes');?>

<?php } else { ?>
      <?php echo l10n('Inactive Themes');?>

<?php }?>
    </legend>
    <div class="themeBoxes">
  <?php $_smarty_tpl->tpl_vars['field_name'] = new Smarty_Variable($_smarty_tpl->tpl_vars['theme']->value['STATE'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'field_name', 0);
}?>

<?php if (!empty($_smarty_tpl->tpl_vars['theme']->value['AUTHOR'])) {
if (!empty($_smarty_tpl->tpl_vars['theme']->value['AUTHOR_URL'])) {?>
      <?php $_smarty_tpl->tpl_vars['author'] = new Smarty_Variable(sprintf("<a href='%s'>%s</a>",$_smarty_tpl->tpl_vars['theme']->value['AUTHOR_URL'],$_smarty_tpl->tpl_vars['theme']->value['AUTHOR']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'author', 0);
} else { ?>
      <?php $_smarty_tpl->tpl_vars['author'] = new Smarty_Variable((('<u>').($_smarty_tpl->tpl_vars['theme']->value['AUTHOR'])).('</u>'), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'author', 0);
}
}
if (!empty($_smarty_tpl->tpl_vars['theme']->value['VISIT_URL'])) {?>
    <?php $_smarty_tpl->tpl_vars['version'] = new Smarty_Variable((((("<a class='externalLink' href='").($_smarty_tpl->tpl_vars['theme']->value['VISIT_URL'])).("'>")).($_smarty_tpl->tpl_vars['theme']->value['VERSION'])).("</a>"), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'version', 0);
} else { ?>
    <?php $_smarty_tpl->tpl_vars['version'] = new Smarty_Variable($_smarty_tpl->tpl_vars['theme']->value['VERSION'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'version', 0);
}?>
  <div class="themeBox<?php if ($_smarty_tpl->tpl_vars['theme']->value['IS_DEFAULT']) {?> themeDefault<?php }?>">
    <div class="themeName">
      <?php echo $_smarty_tpl->tpl_vars['theme']->value['NAME'];?>
 <?php if ($_smarty_tpl->tpl_vars['theme']->value['IS_DEFAULT']) {?><em>(<?php echo l10n('default');?>
)</em><?php }?> <?php if ($_smarty_tpl->tpl_vars['theme']->value['IS_MOBILE']) {?><em>(<?php echo l10n('Mobile');?>
)</em><?php }?>
      <a class="icon-info-circled-1 showInfo" title="<?php if (!empty($_smarty_tpl->tpl_vars['author']->value)) {
echo l10n('By %s',$_smarty_tpl->tpl_vars['author']->value);?>
 | <?php }
echo l10n('Version');?>
 <?php echo $_smarty_tpl->tpl_vars['version']->value;?>
<br/><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['theme']->value['DESC'], ENT_QUOTES, 'UTF-8', true);?>
"></a>
    </div>
    <div class="themeShot"><a href="<?php echo $_smarty_tpl->tpl_vars['theme']->value['SCREENSHOT'];?>
" class="preview-box" title="<?php echo $_smarty_tpl->tpl_vars['theme']->value['NAME'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['theme']->value['SCREENSHOT'];?>
" alt=""></a></div>
    <div class="themeActions">
      <div>
<?php if ($_smarty_tpl->tpl_vars['theme']->value['STATE'] == 'active') {
if ($_smarty_tpl->tpl_vars['theme']->value['DEACTIVABLE']) {?>
      <a href="<?php echo $_smarty_tpl->tpl_vars['deactivate_baseurl']->value;
echo $_smarty_tpl->tpl_vars['theme']->value['ID'];?>
" class="tiptip" title="<?php echo l10n('Forbid this theme to users');?>
"><?php echo l10n('Deactivate');?>
</a>
<?php } else { ?>
      <span title="<?php echo $_smarty_tpl->tpl_vars['theme']->value['DEACTIVATE_TOOLTIP'];?>
" class="tiptip"><?php echo l10n('Deactivate');?>
</span>
<?php }
if (!$_smarty_tpl->tpl_vars['theme']->value['IS_DEFAULT']) {?>
      | <a href="<?php echo $_smarty_tpl->tpl_vars['set_default_baseurl']->value;
echo $_smarty_tpl->tpl_vars['theme']->value['ID'];?>
" class="tiptip" title="<?php echo l10n('Set as default theme for unregistered and new users');?>
"><?php echo l10n('Default');?>
</a>
<?php }
if ($_smarty_tpl->tpl_vars['theme']->value['ADMIN_URI']) {?>
      <br><a href="<?php echo $_smarty_tpl->tpl_vars['theme']->value['ADMIN_URI'];?>
" class="tiptip" title="<?php echo l10n('Configuration');?>
"><?php echo l10n('Configuration');?>
</a>
<?php }
} else {
if ($_smarty_tpl->tpl_vars['theme']->value['ACTIVABLE']) {?>
      <a href="<?php echo $_smarty_tpl->tpl_vars['activate_baseurl']->value;
echo $_smarty_tpl->tpl_vars['theme']->value['ID'];?>
" title="<?php echo l10n('Make this theme available to users');?>
" class="tiptip"><?php echo l10n('Activate');?>
</a>
<?php } else { ?>
      <span title="<?php echo $_smarty_tpl->tpl_vars['theme']->value['ACTIVATE_TOOLTIP'];?>
" class="tiptip"><?php echo l10n('Activate');?>
</span>
<?php }?>
      |
<?php if ($_smarty_tpl->tpl_vars['theme']->value['DELETABLE']) {?>
      <a href="<?php echo $_smarty_tpl->tpl_vars['delete_baseurl']->value;
echo $_smarty_tpl->tpl_vars['theme']->value['ID'];?>
" onclick="return confirm('<?php echo strtr(l10n('Are you sure?'), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');" title="<?php echo l10n('Delete this theme');?>
"><?php echo l10n('Delete');?>
</a>
<?php } else { ?>
      <span title="<?php echo $_smarty_tpl->tpl_vars['theme']->value['DELETE_TOOLTIP'];?>
" class="tiptip"><?php echo l10n('Delete');?>
</span>
<?php }
}?>
      </div>
    </div> <!-- themeActions -->
  </div>
  
<?php
$_smarty_tpl->tpl_vars['theme'] = $__foreach_theme_0_saved_local_item;
}
if ($__foreach_theme_0_saved_item) {
$_smarty_tpl->tpl_vars['theme'] = $__foreach_theme_0_saved_item;
}
?>
</div> <!-- themeBoxes -->
</fieldset>

</div> <!-- themesContent -->
<?php }
}
