<?php
/* Smarty version 3.1.29, created on 2016-09-02 22:09:26
  from "/config/www/gallery/admin/themes/default/template/configuration_sizes.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57c9f896efcf81_00252272',
  'file_dependency' => 
  array (
    '44dbc06b59acc8c93cec6491164b98eeab936f54' => 
    array (
      0 => '/config/www/gallery/admin/themes/default/template/configuration_sizes.tpl',
      1 => 1467916582,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57c9f896efcf81_00252272 ($_smarty_tpl) {
echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['combine_script'][0][0]->func_combine_script(array('id'=>'common','load'=>'footer','path'=>'admin/themes/default/js/common.js'),$_smarty_tpl);?>


<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('footer_script', array()); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['footer_script'][0][0]->block_footer_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

(function(){
  var labelMaxWidth = "<?php echo l10n('Maximum width');?>
",
      labelWidth = "<?php echo l10n('Width');?>
",
      labelMaxHeight = "<?php echo l10n('Maximum height');?>
",
      labelHeight = "<?php echo l10n('Height');?>
";

  function toggleResizeFields(size) {
    var checkbox = jQuery("[name=original_resize]");
    var needToggle = jQuery("#sizeEdit-original");

    if (jQuery(checkbox).is(':checked')) {
      needToggle.show();
    }
    else {
      needToggle.hide();
    }
  }

  toggleResizeFields("original");
  jQuery("[name=original_resize]").click(function () {
    toggleResizeFields("original");
  });

  jQuery("a[id^='sizeEditOpen-']").click(function(){
    var sizeName = jQuery(this).attr("id").split("-")[1];
    jQuery("#sizeEdit-"+sizeName).toggle();
    jQuery(this).hide();
		return false;
  });

  jQuery(".cropToggle").click(function() {
    var labelBoxWidth = jQuery(this).parents('table.sizeEditForm').find('td.sizeEditWidth');
    var labelBoxHeight = jQuery(this).parents('table.sizeEditForm').find('td.sizeEditHeight');

    if (jQuery(this).is(':checked')) {
      jQuery(labelBoxWidth).html(labelWidth);
      jQuery(labelBoxHeight).html(labelHeight);
    }
    else {
      jQuery(labelBoxWidth).html(labelMaxWidth);
      jQuery(labelBoxHeight).html(labelMaxHeight);
    }
  });

  jQuery("#showDetails").click(function() {
    jQuery(".sizeDetails").show();
    jQuery(this).css("visibility", "hidden");
		return false;
  });
}());
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['footer_script'][0][0]->block_footer_script(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('html_style', array()); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['html_style'][0][0]->block_html_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

.sizeEnable { width:50px; }
.sizeEnable .icon-ok { position:relative; left:2px; }
.sizeEditForm { margin:0 0 10px 20px; }
.sizeEdit { display:none; }
#sizesConf table { margin:0; }
.showDetails { padding:0; }
.sizeDetails { display:none;margin-left:10px; }
.sizeEditOpen { margin-left:10px; }
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['html_style'][0][0]->block_html_style(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<h2><?php echo l10n('Piwigo configuration');?>
 <?php echo $_smarty_tpl->tpl_vars['TABSHEET_TITLE']->value;?>
</h2>

<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['F_ACTION']->value;?>
" class="properties">

<div id="configContent">

  <fieldset id="sizesConf">
    <legend><?php echo l10n('Original Size');?>
</legend>
<?php if ($_smarty_tpl->tpl_vars['is_gd']->value) {?>
    <div>
      <?php echo l10n('Resize after upload disabled due to the use of GD as graphic library');?>

      <input type="checkbox" name="original_resize"disabled="disabled" style="visibility: hidden">
      <input type="hidden" name="original_resize_maxwidth" value="<?php echo $_smarty_tpl->tpl_vars['sizes']->value['original_resize_maxwidth'];?>
">
      <input type="hidden" name="original_resize_maxheight" value="<?php echo $_smarty_tpl->tpl_vars['sizes']->value['original_resize_maxheight'];?>
">
      <input type="hidden" name="original_resize_quality" value="<?php echo $_smarty_tpl->tpl_vars['sizes']->value['original_resize_quality'];?>
">
    </div>
<?php } else { ?>
    <div>
      <label class="font-checkbox">
        <span class="icon-check"></span>
        <input type="checkbox" name="original_resize" <?php if (($_smarty_tpl->tpl_vars['sizes']->value['original_resize'])) {?>checked="checked"<?php }?>>
        <?php echo l10n('Resize after upload');?>

      </label>
    </div>

    <table id="sizeEdit-original">
      <tr>
        <th><?php echo l10n('Maximum width');?>
</th>
        <td>
          <input type="text" name="original_resize_maxwidth" value="<?php echo $_smarty_tpl->tpl_vars['sizes']->value['original_resize_maxwidth'];?>
" size="4" maxlength="4"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['original_resize_maxwidth'])) {?> class="dError"<?php }?>> <?php echo l10n('pixels');?>

          <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['original_resize_maxwidth'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value['original_resize_maxwidth'];?>
">!</span><?php }?>
        </td>
      </tr>
      <tr>
        <th><?php echo l10n('Maximum height');?>
</th>
        <td>
          <input type="text" name="original_resize_maxheight" value="<?php echo $_smarty_tpl->tpl_vars['sizes']->value['original_resize_maxheight'];?>
" size="4" maxlength="4"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['original_resize_maxheight'])) {?> class="dError"<?php }?>> <?php echo l10n('pixels');?>

          <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['original_resize_maxheight'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value['original_resize_maxheight'];?>
">!</span><?php }?>
        </td>
      </tr>
      <tr>
        <th><?php echo l10n('Image Quality');?>
</th>
        <td>
          <input type="text" name="original_resize_quality" value="<?php echo $_smarty_tpl->tpl_vars['sizes']->value['original_resize_quality'];?>
" size="3" maxlength="3"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['original_resize_quality'])) {?> class="dError"<?php }?>> %
          <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['original_resize_quality'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value['original_resize_quality'];?>
">!</span><?php }?>
        </td>
      </tr>
    </table>
<?php }?>
  </fieldset>

  <fieldset id="multiSizesConf">
    <legend><?php echo l10n('Multiple Size');?>
</legend>

    <div class="showDetails">
      <a href="#" id="showDetails"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value)) {?> style="display:none"<?php }?>><?php echo l10n('show details');?>
</a>
    </div>

    <table style="margin:0">
<?php
$_from = $_smarty_tpl->tpl_vars['derivatives']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_d_0_saved_item = isset($_smarty_tpl->tpl_vars['d']) ? $_smarty_tpl->tpl_vars['d'] : false;
$__foreach_d_0_saved_key = isset($_smarty_tpl->tpl_vars['type']) ? $_smarty_tpl->tpl_vars['type'] : false;
$_smarty_tpl->tpl_vars['d'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['type'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['d']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['type']->value => $_smarty_tpl->tpl_vars['d']->value) {
$_smarty_tpl->tpl_vars['d']->_loop = true;
$__foreach_d_0_saved_local_item = $_smarty_tpl->tpl_vars['d'];
?>
      <tr>
        <td>
          <label>
<?php if ($_smarty_tpl->tpl_vars['d']->value['must_enable']) {?>
            <span class="sizeEnable">
              <span class="icon-ok"></span>
            </span>
<?php } else { ?>
            <span class="sizeEnable font-checkbox">
              <span class="icon-check"></span>
              <input type="checkbox" name="d[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][enabled]" <?php if ($_smarty_tpl->tpl_vars['d']->value['enabled']) {?>checked="checked"<?php }?>>
            </span>
<?php }?>
            <?php echo l10n($_smarty_tpl->tpl_vars['type']->value);?>

          </label>
        </td>

        <td>
          <span class="sizeDetails"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value)) {?> style="display:inline"<?php }?>><?php echo $_smarty_tpl->tpl_vars['d']->value['w'];?>
 x <?php echo $_smarty_tpl->tpl_vars['d']->value['h'];?>
 <?php echo l10n('pixels');
if ($_smarty_tpl->tpl_vars['d']->value['crop']) {?>, <?php echo mb_strtolower(l10n('Crop'), 'UTF-8');
}?></span>
        </td>

        <td>
          <span class="sizeDetails"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value) && !isset($_smarty_tpl->tpl_vars['ferrors']->value[$_smarty_tpl->tpl_vars['type']->value])) {?> style="display:inline"<?php }?>>
            <a href="#" id="sizeEditOpen-<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
" class="sizeEditOpen"><?php echo l10n('edit');?>
</a>
          </span>
        </td>
      </tr>

      <tr id="sizeEdit-<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
" class="sizeEdit" <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value[$_smarty_tpl->tpl_vars['type']->value])) {?> style="display:block"<?php }?>>
        <td colspan="3">
          <table class="sizeEditForm">
<?php if (!$_smarty_tpl->tpl_vars['d']->value['must_square']) {?>
            <tr>
              <td colspan="2">
                <label class="font-checkbox">
                <span class="icon-check"></span>
                <input type="checkbox" class="cropToggle" name="d[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][crop]" <?php if ($_smarty_tpl->tpl_vars['d']->value['crop']) {?>checked="checked"<?php }?>>
                  <?php echo l10n('Crop');?>

                </label>
              </td>
            </tr>
<?php }?>
            <tr>
              <td class="sizeEditWidth"><?php if ($_smarty_tpl->tpl_vars['d']->value['must_square'] || $_smarty_tpl->tpl_vars['d']->value['crop']) {
echo l10n('Width');
} else {
echo l10n('Maximum width');
}?></td>
              <td>
                <input type="text" name="d[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][w]" maxlength="4" size="4" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['w'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value[$_smarty_tpl->tpl_vars['type']->value]['w'])) {?> class="dError"<?php }?>> <?php echo l10n('pixels');?>

                <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value[$_smarty_tpl->tpl_vars['type']->value]['w'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value[$_smarty_tpl->tpl_vars['type']->value]['w'];?>
">!</span><?php }?>
              </td>
            </tr>
<?php if (!$_smarty_tpl->tpl_vars['d']->value['must_square']) {?>
            <tr>
              <td class="sizeEditHeight"><?php if ($_smarty_tpl->tpl_vars['d']->value['crop']) {
echo l10n('Height');
} else {
echo l10n('Maximum height');
}?></td>
              <td>
                <input type="text" name="d[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][h]" maxlength="4" size="4"  value="<?php echo $_smarty_tpl->tpl_vars['d']->value['h'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value[$_smarty_tpl->tpl_vars['type']->value]['h'])) {?> class="dError"<?php }?>> <?php echo l10n('pixels');?>

                <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value[$_smarty_tpl->tpl_vars['type']->value]['h'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value[$_smarty_tpl->tpl_vars['type']->value]['h'];?>
">!</span><?php }?>
              </td>
            </tr>
<?php }?>
            <tr>
              <td><?php echo l10n('Sharpen');?>
</td>
              <td>
                <input type="text" name="d[<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
][sharpen]" maxlength="4" size="4"  value="<?php echo $_smarty_tpl->tpl_vars['d']->value['sharpen'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value[$_smarty_tpl->tpl_vars['type']->value]['sharpen'])) {?> class="dError"<?php }?>> %
                <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value[$_smarty_tpl->tpl_vars['type']->value]['sharpen'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value[$_smarty_tpl->tpl_vars['type']->value]['sharpen'];?>
">!</span><?php }?>
              </td>
            </tr>
          </table> 
        </td>
      </tr>
<?php
$_smarty_tpl->tpl_vars['d'] = $__foreach_d_0_saved_local_item;
}
if ($__foreach_d_0_saved_item) {
$_smarty_tpl->tpl_vars['d'] = $__foreach_d_0_saved_item;
}
if ($__foreach_d_0_saved_key) {
$_smarty_tpl->tpl_vars['type'] = $__foreach_d_0_saved_key;
}
?>
    </table>

    <p style="margin:10px 0 0 0;<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value)) {?> display:block;<?php }?>" class="sizeDetails">
      <?php echo l10n('Image Quality');?>

      <input type="text" name="resize_quality" value="<?php echo $_smarty_tpl->tpl_vars['resize_quality']->value;?>
" size="3" maxlength="3"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['resize_quality'])) {?> class="dError"<?php }?>> %
      <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['resize_quality'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value['resize_quality'];?>
">!</span><?php }?>
    </p>
    <p style="margin:10px 0 0 0;<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value)) {?> display:block;<?php }?>" class="sizeDetails">
      <a href="<?php echo $_smarty_tpl->tpl_vars['F_ACTION']->value;?>
&action=restore_settings" onclick="return confirm('<?php echo strtr(l10n('Are you sure?'), array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');"><?php echo l10n('Reset to default values');?>
</a>
    </p>

<?php if (!empty($_smarty_tpl->tpl_vars['custom_derivatives']->value)) {?>
    <fieldset class="sizeDetails">
      <legend><?php echo l10n('custom');?>
</legend>

      <table style="margin:0">
<?php
$_from = $_smarty_tpl->tpl_vars['custom_derivatives']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_time_1_saved_item = isset($_smarty_tpl->tpl_vars['time']) ? $_smarty_tpl->tpl_vars['time'] : false;
$__foreach_time_1_saved_key = isset($_smarty_tpl->tpl_vars['custom']) ? $_smarty_tpl->tpl_vars['custom'] : false;
$_smarty_tpl->tpl_vars['time'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['custom'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['time']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['custom']->value => $_smarty_tpl->tpl_vars['time']->value) {
$_smarty_tpl->tpl_vars['time']->_loop = true;
$__foreach_time_1_saved_local_item = $_smarty_tpl->tpl_vars['time'];
?>
        <tr><td>
          <label class="font-checkbox">
            <span class="icon-check"></span>
            <input type="checkbox" name="delete_custom_derivative_<?php echo $_smarty_tpl->tpl_vars['custom']->value;?>
"> <?php echo l10n('Delete');?>
 <?php echo $_smarty_tpl->tpl_vars['custom']->value;?>
 (<?php echo l10n('Last hit');?>
: <?php echo $_smarty_tpl->tpl_vars['time']->value;?>
)
          </label>
        </td></tr>
<?php
$_smarty_tpl->tpl_vars['time'] = $__foreach_time_1_saved_local_item;
}
if ($__foreach_time_1_saved_item) {
$_smarty_tpl->tpl_vars['time'] = $__foreach_time_1_saved_item;
}
if ($__foreach_time_1_saved_key) {
$_smarty_tpl->tpl_vars['custom'] = $__foreach_time_1_saved_key;
}
?>
      </table>
    </fieldset>
<?php }?>
  </fieldset>

</div> <!-- configContent -->

<p class="formButtons">
  <input type="submit" name="submit" value="<?php echo l10n('Save Settings');?>
">
</p>

</form><?php }
}
