<?php
/* Smarty version 3.1.29, created on 2016-09-02 22:09:03
  from "/config/www/gallery/admin/themes/default/template/configuration_main.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57c9f87f9b2248_52090385',
  'file_dependency' => 
  array (
    '4b5f42b9ecdab7f2b419261a829d3f7eb1bee788' => 
    array (
      0 => '/config/www/gallery/admin/themes/default/template/configuration_main.tpl',
      1 => 1467916582,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/colorbox.inc.tpl' => 1,
  ),
),false)) {
function content_57c9f87f9b2248_52090385 ($_smarty_tpl) {
if (!is_callable('smarty_function_html_options')) require_once '/config/www/gallery/include/smarty/libs/plugins/function.html_options.php';
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/colorbox.inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['combine_script'][0][0]->func_combine_script(array('id'=>'common','load'=>'footer','path'=>'admin/themes/default/js/common.js'),$_smarty_tpl);?>


<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('footer_script', array('require'=>'jquery')); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['footer_script'][0][0]->block_footer_script(array('require'=>'jquery'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

(function(){
  var targets = {
    'input[name="rate"]' : '#rate_anonymous',
    'input[name="allow_user_registration"]' : '#email_admin_on_new_user'
  };

  for (selector in targets) {
    var target = targets[selector];

    jQuery(target).toggle(jQuery(selector).is(':checked'));

    (function(target){
      jQuery(selector).on('change', function() {
        jQuery(target).toggle($(this).is(':checked'));
      });
    })(target);
  };
}());

<?php if (!isset($_smarty_tpl->tpl_vars['ORDER_BY_IS_CUSTOM']->value)) {?>
(function(){
  var max_fields = Math.ceil(<?php echo count($_smarty_tpl->tpl_vars['main']->value['order_by_options']);?>
/2);

  function updateFilters() {
    var $selects = jQuery('#order_filters select');

    jQuery('#order_filters .addFilter').toggle($selects.length <= max_fields);
    jQuery('#order_filters .removeFilter').css('display', '').filter(':first').css('display', 'none');

    $selects.find('option').removeAttr('disabled');
    $selects.each(function() {
      $selects.not(this).find('option[value="'+ jQuery(this).val() +'"]').attr('disabled', 'disabled');
    });
  }

  jQuery('#order_filters').on('click', '.removeFilter', function() {
    jQuery(this).parent('span.filter').remove();
    updateFilters();
  });

  jQuery('#order_filters').on('change', 'select', updateFilters);

  jQuery('#order_filters .addFilter').click(function() {
    jQuery(this).prev('span.filter').clone().insertBefore(jQuery(this));
    jQuery(this).prev('span.filter').children('select').val('');
    updateFilters();
  });

  updateFilters();
}());
<?php }?>

jQuery(".themeBoxes a").colorbox();

jQuery("input[name='mail_theme']").change(function() {
  jQuery("input[name='mail_theme']").parents(".themeBox").removeClass("themeDefault");
  jQuery(this).parents(".themeBox").addClass("themeDefault");
});
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['footer_script'][0][0]->block_footer_script(array('require'=>'jquery'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<h2><?php echo l10n('Piwigo configuration');?>
 <?php echo $_smarty_tpl->tpl_vars['TABSHEET_TITLE']->value;?>
</h2>

<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['F_ACTION']->value;?>
" class="properties">

<div id="configContent">

  <fieldset class="mainConf">
    <legend><?php echo l10n('Basic settings');?>
</legend>
    <ul>
      <li>
        <label for="gallery_title"><?php echo l10n('Gallery title');?>
</label>
        <br>
        <input type="text" maxlength="255" size="50" name="gallery_title" id="gallery_title" value="<?php echo $_smarty_tpl->tpl_vars['main']->value['CONF_GALLERY_TITLE'];?>
">
      </li>

      <li>
        <label for="page_banner"><?php echo l10n('Page banner');?>
</label>
        <br>
        <textarea rows="5" cols="50" class="description" name="page_banner" id="page_banner"><?php echo $_smarty_tpl->tpl_vars['main']->value['CONF_PAGE_BANNER'];?>
</textarea>
      </li>

     <li id="order_filters">
        <label><?php echo l10n('Default photos order');?>
</label>

<?php
$_from = $_smarty_tpl->tpl_vars['main']->value['order_by'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_order_0_saved_item = isset($_smarty_tpl->tpl_vars['order']) ? $_smarty_tpl->tpl_vars['order'] : false;
$_smarty_tpl->tpl_vars['order'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['order']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->_loop = true;
$__foreach_order_0_saved_local_item = $_smarty_tpl->tpl_vars['order'];
?>
        <span class="filter <?php if (isset($_smarty_tpl->tpl_vars['ORDER_BY_IS_CUSTOM']->value)) {?>transparent<?php }?>">
          <select name="order_by[]" <?php if (isset($_smarty_tpl->tpl_vars['ORDER_BY_IS_CUSTOM']->value)) {?>disabled<?php }?>>
            <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['main']->value['order_by_options'],'selected'=>$_smarty_tpl->tpl_vars['order']->value),$_smarty_tpl);?>

          </select>
          <a class="removeFilter"><?php echo l10n('delete');?>
</a>
        </span>
<?php
$_smarty_tpl->tpl_vars['order'] = $__foreach_order_0_saved_local_item;
}
if ($__foreach_order_0_saved_item) {
$_smarty_tpl->tpl_vars['order'] = $__foreach_order_0_saved_item;
}
if (!isset($_smarty_tpl->tpl_vars['ORDER_BY_IS_CUSTOM']->value)) {?>
        <a class="addFilter"><?php echo l10n('Add a criteria');?>
</a>
<?php } else { ?>
        <span class="order_by_is_custom"><?php echo l10n('You can\'t define a default photo order because you have a custom setting in your local configuration.');?>
</span>
<?php }?>
      </li>
    </ul>
  </fieldset>

  <fieldset class="mainConf">
    <legend><?php echo l10n('Permissions');?>
</legend>
    <ul>
      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="rate" <?php if (($_smarty_tpl->tpl_vars['main']->value['rate'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Allow rating');?>

        </label>

        <label id="rate_anonymous" class="font-checkbox no-bold">
          <span class="icon-check"></span>
          <input type="checkbox" name="rate_anonymous" <?php if (($_smarty_tpl->tpl_vars['main']->value['rate_anonymous'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Rating by guests');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="allow_user_registration" <?php if (($_smarty_tpl->tpl_vars['main']->value['allow_user_registration'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Allow user registration');?>

        </label>

        <label id="email_admin_on_new_user" class="font-checkbox no-bold">
          <span class="icon-check"></span>
          <input type="checkbox" name="email_admin_on_new_user" <?php if (($_smarty_tpl->tpl_vars['main']->value['email_admin_on_new_user'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Email admins when a new user registers');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="allow_user_customization" <?php if (($_smarty_tpl->tpl_vars['main']->value['allow_user_customization'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Allow user customization');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="obligatory_user_mail_address" <?php if (($_smarty_tpl->tpl_vars['main']->value['obligatory_user_mail_address'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Mail address is mandatory for registration');?>

        </label>
      </li>
    </ul>
  </fieldset>

  <fieldset class="mainConf">
    <legend><?php echo l10n('Miscellaneous');?>
</legend>
    <ul>
      <li>
        <label><?php echo l10n('Week starts on');?>

        <?php echo smarty_function_html_options(array('name'=>"week_starts_on",'options'=>$_smarty_tpl->tpl_vars['main']->value['week_starts_on_options'],'selected'=>$_smarty_tpl->tpl_vars['main']->value['week_starts_on_options_selected']),$_smarty_tpl);?>
</label>
      </li>

      <li>
        <strong><?php echo l10n('Save visits in history for');?>
</strong>

        <label class="font-checkbox no-bold">
          <span class="icon-check"></span>
          <input type="checkbox" name="history_guest" <?php if (($_smarty_tpl->tpl_vars['main']->value['history_guest'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('simple visitors');?>

        </label>

        <label class="font-checkbox no-bold">
          <span class="icon-check"></span>
          <input type="checkbox" name="log" <?php if (($_smarty_tpl->tpl_vars['main']->value['log'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('registered users');?>

        </label>

        <label class="font-checkbox no-bold">
          <span class="icon-check"></span>
          <input type="checkbox" name="history_admin" <?php if (($_smarty_tpl->tpl_vars['main']->value['history_admin'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('administrators');?>

        </label>
      </li>

      <li>
        <label><?php echo l10n('Mail theme');?>
</label>

        <div class="themeBoxes font-checkbox">
<?php
$_from = $_smarty_tpl->tpl_vars['main']->value['mail_theme_options'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_name_1_saved_item = isset($_smarty_tpl->tpl_vars['name']) ? $_smarty_tpl->tpl_vars['name'] : false;
$__foreach_name_1_saved_key = isset($_smarty_tpl->tpl_vars['theme']) ? $_smarty_tpl->tpl_vars['theme'] : false;
$_smarty_tpl->tpl_vars['name'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['theme'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['name']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['theme']->value => $_smarty_tpl->tpl_vars['name']->value) {
$_smarty_tpl->tpl_vars['name']->_loop = true;
$__foreach_name_1_saved_local_item = $_smarty_tpl->tpl_vars['name'];
?>
          <div class="themeBox <?php if ($_smarty_tpl->tpl_vars['main']->value['mail_theme'] == $_smarty_tpl->tpl_vars['theme']->value) {?>themeDefault<?php }?>">
            <label>
              <div class="themeName">
                <span class="icon-check"></span>
                <input type="radio" name="mail_theme" value="<?php echo $_smarty_tpl->tpl_vars['theme']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['main']->value['mail_theme'] == $_smarty_tpl->tpl_vars['theme']->value) {?>checked<?php }?>>
                <?php echo $_smarty_tpl->tpl_vars['name']->value;?>

              </div>
              <div class="themeShot">
                <img src="<?php echo $_smarty_tpl->tpl_vars['ROOT_URL']->value;?>
themes/default/template/mail/screenshot-<?php echo $_smarty_tpl->tpl_vars['theme']->value;?>
.png" width="150"/>
              </div>
            </label>
            <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT_URL']->value;?>
themes/default/template/mail/screenshot-<?php echo $_smarty_tpl->tpl_vars['theme']->value;?>
.png"><?php echo l10n('Preview');?>
</a>
          </div>
<?php
$_smarty_tpl->tpl_vars['name'] = $__foreach_name_1_saved_local_item;
}
if ($__foreach_name_1_saved_item) {
$_smarty_tpl->tpl_vars['name'] = $__foreach_name_1_saved_item;
}
if ($__foreach_name_1_saved_key) {
$_smarty_tpl->tpl_vars['theme'] = $__foreach_name_1_saved_key;
}
?>
        </div>
      </li>
    </ul>
  </fieldset>

</div> <!-- configContent -->

<p class="formButtons">
  <input type="submit" name="submit" value="<?php echo l10n('Save Settings');?>
">
</p>

</form><?php }
}
