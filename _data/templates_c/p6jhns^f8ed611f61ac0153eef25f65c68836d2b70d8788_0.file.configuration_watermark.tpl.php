<?php
/* Smarty version 3.1.29, created on 2016-09-02 22:09:31
  from "/config/www/gallery/admin/themes/default/template/configuration_watermark.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57c9f89b4cd1f7_95944568',
  'file_dependency' => 
  array (
    'f8ed611f61ac0153eef25f65c68836d2b70d8788' => 
    array (
      0 => '/config/www/gallery/admin/themes/default/template/configuration_watermark.tpl',
      1 => 1467916582,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57c9f89b4cd1f7_95944568 ($_smarty_tpl) {
if (!is_callable('smarty_function_html_options')) require_once '/config/www/gallery/include/smarty/libs/plugins/function.html_options.php';
echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['combine_script'][0][0]->func_combine_script(array('id'=>'common','load'=>'footer','path'=>'admin/themes/default/js/common.js'),$_smarty_tpl);?>


<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('footer_script', array()); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['footer_script'][0][0]->block_footer_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

(function(){
  function onWatermarkChange() {
    var val = jQuery("#wSelect").val();
    if (val.length) {
      jQuery("#wImg").attr('src', '<?php echo $_smarty_tpl->tpl_vars['ROOT_URL']->value;?>
'+val).show();
    }
    else {
      jQuery("#wImg").hide();
    }
  }

  onWatermarkChange();

  jQuery("#wSelect").bind("change", onWatermarkChange);

  if (jQuery("input[name='w[position]']:checked").val() == 'custom') {
    jQuery("#positionCustomDetails").show();
  }

  jQuery("input[name='w[position]']").change(function(){
    if (jQuery(this).val() == 'custom') {
      jQuery("#positionCustomDetails").show();
    }
    else {
      jQuery("#positionCustomDetails").hide();
    }
  });

  jQuery(".addWatermarkOpen").click(function(){
    jQuery("#addWatermark, #selectWatermark").toggle();
		return false;
  });
}());
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['footer_script'][0][0]->block_footer_script(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<h2><?php echo l10n('Piwigo configuration');?>
 <?php echo $_smarty_tpl->tpl_vars['TABSHEET_TITLE']->value;?>
</h2>

<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['F_ACTION']->value;?>
" class="properties" enctype="multipart/form-data">

<div id="configContent">

  <fieldset id="watermarkConf" class="no-border">
    <legend></legend>
    <ul>
      <li>
        <span id="selectWatermark"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermarkImage'])) {?> style="display:none"<?php }?>><label><?php echo l10n('Select a file');?>
</label>
          <select name="w[file]" id="wSelect">
            <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['watermark_files']->value,'selected'=>$_smarty_tpl->tpl_vars['watermark']->value['file']),$_smarty_tpl);?>

          </select>

          <?php echo l10n('... or ');?>
<a href="#" class="addWatermarkOpen"><?php echo l10n('add a new watermark');?>
</a>
          <br>
          <img id="wImg"></img>
        </span>

        <span id="addWatermark"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermarkImage'])) {?> style="display:inline"<?php }?>>
          <?php echo l10n('add a new watermark');?>
 <?php echo l10n('... or ');?>
<a href="#" class="addWatermarkOpen"><?php echo l10n('Select a file');?>
</a>

          <br>
          <input type="file" size="60" id="watermarkImage" name="watermarkImage"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermarkImage'])) {?> class="dError"<?php }?>> (png)
          <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermarkImage'])) {?><span class="dErrorDesc" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ferrors']->value['watermarkImage']);?>
">!</span><?php }?>
        </span>
      </li>

      <li>
        <label>
          <?php echo l10n('Apply watermark if width is bigger than');?>

          <input  size="4" maxlength="4" type="text" name="w[minw]" value="<?php echo $_smarty_tpl->tpl_vars['watermark']->value['minw'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['minw'])) {?> class="dError"<?php }?>>
        </label>
        <?php echo l10n('pixels');?>

      </li>

      <li>
        <label>
          <?php echo l10n('Apply watermark if height is bigger than');?>

          <input  size="4" maxlength="4" type="text" name="w[minh]" value="<?php echo $_smarty_tpl->tpl_vars['watermark']->value['minh'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['minh'])) {?> class="dError"<?php }?>>
        </label>
        <?php echo l10n('pixels');?>

      </li>

      <li>
        <label><?php echo l10n('Position');?>
</label>
        <br>
        <div id="watermarkPositionBox">
          <label class="right"><?php echo l10n('top right corner');?>
 <input name="w[position]" type="radio" value="topright"<?php if ($_smarty_tpl->tpl_vars['watermark']->value['position'] == 'topright') {?> checked="checked"<?php }?>></label>
          <label><input name="w[position]" type="radio" value="topleft"<?php if ($_smarty_tpl->tpl_vars['watermark']->value['position'] == 'topleft') {?> checked="checked"<?php }?>> <?php echo l10n('top left corner');?>
</label>
          <label class="middle"><input name="w[position]" type="radio" value="middle"<?php if ($_smarty_tpl->tpl_vars['watermark']->value['position'] == 'middle') {?> checked="checked"<?php }?>> <?php echo l10n('middle');?>
</label>
          <label class="right"><?php echo l10n('bottom right corner');?>
 <input name="w[position]" type="radio" value="bottomright"<?php if ($_smarty_tpl->tpl_vars['watermark']->value['position'] == 'bottomright') {?> checked="checked"<?php }?>></label>
          <label><input name="w[position]" type="radio" value="bottomleft"<?php if ($_smarty_tpl->tpl_vars['watermark']->value['position'] == 'bottomleft') {?> checked="checked"<?php }?>> <?php echo l10n('bottom left corner');?>
</label>
        </div>

        <label style="display:block;margin-top:10px;font-weight:normal;"><input name="w[position]" type="radio" value="custom"<?php if ($_smarty_tpl->tpl_vars['watermark']->value['position'] == 'custom') {?> checked="checked"<?php }?>> <?php echo l10n('custom');?>
</label>

        <div id="positionCustomDetails">
          <label><?php echo l10n('X Position');?>

            <input size="3" maxlength="3" type="text" name="w[xpos]" value="<?php echo $_smarty_tpl->tpl_vars['watermark']->value['xpos'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['xpos'])) {?> class="dError"<?php }?>>%
            <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['xpos'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value['watermark']['xpos'];?>
">!</span><?php }?>
          </label>

          <br>
          <label><?php echo l10n('Y Position');?>

            <input size="3" maxlength="3" type="text" name="w[ypos]" value="<?php echo $_smarty_tpl->tpl_vars['watermark']->value['ypos'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['ypos'])) {?> class="dError"<?php }?>>%
            <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['ypos'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value['watermark']['ypos'];?>
">!</span><?php }?>
          </label>

          <br>
          <label><?php echo l10n('X Repeat');?>

            <input size="3" maxlength="3" type="text" name="w[xrepeat]" value="<?php echo $_smarty_tpl->tpl_vars['watermark']->value['xrepeat'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['xrepeat'])) {?> class="dError"<?php }?>>
            <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['xrepeat'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value['watermark']['xrepeat'];?>
">!</span><?php }?>
          </label>

					<br>
					<label><?php echo l10n('Y Repeat');?>

						<input size="3" maxlength="3" type="text" name="w[yrepeat]" value="<?php echo $_smarty_tpl->tpl_vars['watermark']->value['yrepeat'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['yrepeat'])) {?> class="dError"<?php }?>>
						<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['yrepeat'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value['watermark']['yrepeat'];?>
">!</span><?php }?>
					</label>

        </div>
      </li>

      <li>
        <label><?php echo l10n('Opacity');?>
</label>
        <input size="3" maxlength="3" type="text" name="w[opacity]" value="<?php echo $_smarty_tpl->tpl_vars['watermark']->value['opacity'];?>
"<?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['opacity'])) {?> class="dError"<?php }?>> %
        <?php if (isset($_smarty_tpl->tpl_vars['ferrors']->value['watermark']['opacity'])) {?><span class="dErrorDesc" title="<?php echo $_smarty_tpl->tpl_vars['ferrors']->value['watermark']['opacity'];?>
">!</span><?php }?>
      </li>
    </ul>
  </fieldset>

</div> <!-- configContent -->

<p class="formButtons">
  <input type="submit" name="submit" value="<?php echo l10n('Save Settings');?>
">
</p>

</form><?php }
}
