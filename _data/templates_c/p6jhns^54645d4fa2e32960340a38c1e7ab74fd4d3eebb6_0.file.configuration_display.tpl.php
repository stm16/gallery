<?php
/* Smarty version 3.1.29, created on 2016-09-02 22:11:16
  from "/config/www/gallery/admin/themes/default/template/configuration_display.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57c9f90418ffc9_53559296',
  'file_dependency' => 
  array (
    '54645d4fa2e32960340a38c1e7ab74fd4d3eebb6' => 
    array (
      0 => '/config/www/gallery/admin/themes/default/template/configuration_display.tpl',
      1 => 1467916582,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57c9f90418ffc9_53559296 ($_smarty_tpl) {
echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['combine_script'][0][0]->func_combine_script(array('id'=>'common','load'=>'footer','path'=>'admin/themes/default/js/common.js'),$_smarty_tpl);?>


<h2><?php echo l10n('Piwigo configuration');?>
 <?php echo $_smarty_tpl->tpl_vars['TABSHEET_TITLE']->value;?>
</h2>

<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['F_ACTION']->value;?>
" class="properties">

<div id="configContent">

  <fieldset id="indexDisplayConf">
    <legend><?php echo l10n('Main Page');?>
</legend>
    <ul>
      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="menubar_filter_icon" <?php if (($_smarty_tpl->tpl_vars['display']->value['menubar_filter_icon'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate icon "%s"',(ucfirst(l10n('display only recently posted photos'))));?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="index_new_icon" <?php if (($_smarty_tpl->tpl_vars['display']->value['index_new_icon'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate icon "new" next to albums and pictures');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="index_sort_order_input" <?php if (($_smarty_tpl->tpl_vars['display']->value['index_sort_order_input'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate icon "%s"',(l10n('Sort order')));?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="index_flat_icon" <?php if (($_smarty_tpl->tpl_vars['display']->value['index_flat_icon'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate icon "%s"',(ucfirst(l10n('display all photos in all sub-albums'))));?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="index_posted_date_icon" <?php if (($_smarty_tpl->tpl_vars['display']->value['index_posted_date_icon'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate icon "%s"',(ucfirst(l10n('display a calendar by posted date'))));?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="index_created_date_icon" <?php if (($_smarty_tpl->tpl_vars['display']->value['index_created_date_icon'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate icon "%s"',(ucfirst(l10n('display a calendar by creation date'))));?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="index_slideshow_icon" <?php if (($_smarty_tpl->tpl_vars['display']->value['index_slideshow_icon'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate icon "%s"',(ucfirst(l10n('slideshow'))));?>

        </label>
      </li>

      <li>
        <label>
          <?php echo l10n('Number of albums per page');?>

          <input type="text" size="3" maxlength="4" name="nb_categories_page" id="nb_categories_page" value="<?php echo $_smarty_tpl->tpl_vars['display']->value['NB_CATEGORIES_PAGE'];?>
">
        </label>
      </li>
    </ul>
  </fieldset>

  <fieldset id="pictureDisplayConf">
    <legend><?php echo l10n('Photo Page');?>
</legend>
    <ul>
      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_slideshow_icon" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_slideshow_icon'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate icon "%s"',(ucfirst(l10n('slideshow'))));?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_metadata_icon" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_metadata_icon'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate icon "%s"',(l10n('Show file metadata')));?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_download_icon" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_download_icon'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate icon "%s"',(ucfirst(l10n('Download this file'))));?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_favorite_icon" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_favorite_icon'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate icon "%s"',(ucfirst(l10n('add this photo to your favorites'))));?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_navigation_icons" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_navigation_icons'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate Navigation Bar');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_navigation_thumb" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_navigation_thumb'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Activate Navigation Thumbnails');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_menu" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_menu'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Show menubar');?>

        </label>
      </li>
    </ul>
  </fieldset>

  <fieldset id="pictureInfoConf">
    <legend><?php echo l10n('Photo Properties');?>
</legend>
    <ul>
      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_informations[author]" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_informations']['author'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Author');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_informations[created_on]" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_informations']['created_on'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Created on');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_informations[posted_on]" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_informations']['posted_on'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Posted on');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_informations[dimensions]" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_informations']['dimensions'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Dimensions');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_informations[file]" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_informations']['file'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('File');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_informations[filesize]" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_informations']['filesize'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Filesize');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_informations[tags]" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_informations']['tags'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Tags');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_informations[categories]" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_informations']['categories'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Albums');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_informations[visits]" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_informations']['visits'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Visits');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_informations[rating_score]" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_informations']['rating_score'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Rating score');?>

        </label>
      </li>

      <li>
        <label class="font-checkbox">
          <span class="icon-check"></span>
          <input type="checkbox" name="picture_informations[privacy_level]" <?php if (($_smarty_tpl->tpl_vars['display']->value['picture_informations']['privacy_level'])) {?>checked="checked"<?php }?>>
          <?php echo l10n('Who can see this photo?');?>
 (<?php echo l10n('available for administrators only');?>
)
        </label>
      </li>
    </ul>
  </fieldset>

</div> <!-- configContent -->

<p class="formButtons">
  <input type="submit" name="submit" value="<?php echo l10n('Save Settings');?>
">
</p>

</form><?php }
}
